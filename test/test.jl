# ===========================================================
#  Copyright (C) 2022 Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
# 
#  This file is part of bv_remote.
# 
#  bv_remote is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  bv_remote is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
using Test, bv_remote, Sockets

@testset "standard session" begin
  con = bv_remote.open_connection(ip"130.92.154.189", 6700)
  @test con != nothing
  
  bv_remote.set_amplifier(con, bv_remote.select_brainamp_family)
  bv_remote.set_workspace(con, "")
  bv_remote.set_experiment_id(con)
  bv_remote.set_subject_id(con)
  bv_remote.start_recorder(con)

end


