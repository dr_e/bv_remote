# ===========================================================
#  Copyright (C) 2022 Matthias Ertl
#  matthias.ertl@unibe.ch
# 
#  This file is part of bv_remote.
# 
#  bv_remote is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  bv_remote is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc.  If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================

function open_connection(srv = ip"130.92.154.189", port = 6700)
  con = connect(srv,port)
  return con
end

function srv_response(con)
  msg = read(con)
  println(msg)
end

function set_amplifier(con, type::String = select_brainamp_family)
  write(con, type)
  srv_response(con)
end

function set_workspace(con, path::String = ".")
  write(con, @sprintf("1:%s", path))
  srv_response(con)
end

function set_experiment_id(con, name::String = "Test")
  write(con, @sprintf("2:%s", name))
  srv_response(con)
end

function set_subject_id(con, name::String = "VP_test")
  write(con, @sprintf("3:%s", name))
  srv_response(con)
end

function start_recorder(con)
  write(con, "O")
  srv_response(con)
end

function select_mode(con, mode)
  write(con, mode)
  srv_response(con)
end


