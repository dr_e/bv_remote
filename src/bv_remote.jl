# ===========================================================
#  Copyright (C) 2022 Matthias Ertl
#  matthias.ertl@unibe.ch
# 
#  This file is part of bv_remote.
# 
#  bv_remote is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  bv_remote is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc.  If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
module bv_remote 

using Base:@kwdef
using Printf, Sockets

include("open_connection.jl")


const ack = "OK"
const select_actichamp = "SA:actiCHamp\r"
const select_brainamp_family = "SA:BrainAmp Family\r"
const select_liveamp = "SA:LiveAmp\r"
const select_quickamp_usb = "SA:QuickAmp USB\r"
const select_simulated = "SA:Simulated Amplifier\r"
const select_vamp = "SA:V-Amp\r"
const select_firstamp = "SA:FirstAmp\r"





end # module
